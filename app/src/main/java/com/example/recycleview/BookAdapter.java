package com.example.recycleview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    private Context mCtx;
    private List<Book> bookList;

    public BookAdapter(Context mCtx, List<Book> bookList){
        this.mCtx = mCtx;
        this.bookList = bookList;
    }


    @NonNull
    @Override
    public BookAdapter.BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mCtx);
        View view = layoutInflater.inflate(R.layout.view_design,null);
        return new BookViewHolder(view);
    }

    public class BookViewHolder  extends RecyclerView.ViewHolder{
        TextView judul, penulis;
        ImageView imageView;
        public BookViewHolder(View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.judul);
            penulis = itemView.findViewById(R.id.penulis);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull BookAdapter.BookViewHolder holder, int position) {
        Book book = bookList.get(position);
        holder.judul.setText(book.getJudul());
        holder.penulis.setText(book.getPenulis());
        holder.imageView.setImageDrawable(mCtx.getResources().getDrawable(book.getImage()));

    }


    @Override
    public int getItemCount() {
        return bookList.size();
    }


}
