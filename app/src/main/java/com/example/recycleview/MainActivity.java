package com.example.recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Book> bookList;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recylerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        bookList = new ArrayList<>();

        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));


        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));
        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));
        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));
        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));
        bookList.add(new Book("Crazy Rich Asian", "Kevin Kwan", R.drawable.crazy_rich));
        bookList.add(new Book("Sebatas Mimpi", "Hujan Mimpi", R.drawable.sebatas_mimpi));


        BookAdapter adapter = new BookAdapter(this, bookList);
        recyclerView.setAdapter(adapter);

    }
}