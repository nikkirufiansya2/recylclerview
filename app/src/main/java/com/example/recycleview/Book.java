package com.example.recycleview;

public class Book {
    private String judul;
    private String penulis;
    private int image;

    public Book(String judul, String penulis, int image){
        this.judul = judul;
        this.penulis = penulis;
        this.image = image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }



}
